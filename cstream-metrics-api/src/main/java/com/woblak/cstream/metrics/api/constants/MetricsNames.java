package com.woblak.cstream.metrics.api.constants;

public abstract class MetricsNames {

    public static final String INGESTER_CRYPTOWAT_OUT_METRIC = "cstream_ingesterCryptowat_out";
    public static final String INGESTER_OUT_METRIC = "cstream_ingester_out";
    public static final String AGG_SUMMARY_OUT_METRIC = "cstream_aggSummary_out";
    public static final String AGG_MEAN_METRIC = "cstream_aggMean_out";
    public static final String SSE_UPDATED_METRIC = "cstream_sseUpdated_out";
    public static final String SSE_EMITTED_METRIC = "cstream_sseEmitted_out";
}
