package com.woblak.cstream.metrics.core.io.kafka.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
public class KafkaTopicConfig {

    private final String bootstrapAddress;

    public KafkaTopicConfig(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress
    ) {
        this.bootstrapAddress = bootstrapAddress;
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = topicConfigs();
        return new KafkaAdmin(configs);
    }

    @Bean
    public Map<String, Object> topicConfigs() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }

    @Bean
    public Map<String, Object> topicConfigs(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress
    ) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }
}