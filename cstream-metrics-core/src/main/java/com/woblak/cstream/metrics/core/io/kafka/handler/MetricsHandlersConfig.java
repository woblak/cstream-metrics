package com.woblak.cstream.metrics.core.io.kafka.handler;

import com.woblak.cstream.aggregator.api.message.MeanTradePriceMsg;
import com.woblak.cstream.aggregator.api.message.SummaryTradeMsg;
import com.woblak.cstream.ingester.api.message.CryptoTradeMsg;
import com.woblak.cstream.ingester.cryptowat.api.message.CryptowatTradeMsg;
import com.woblak.cstream.metrics.api.constants.MetricsNames;
import com.woblak.cstream.provider.api.message.SseUpdatedEventMsg;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsHandlersConfig {

    private static final double[] DEFAULT_PERCENTILES = new double[]{.5, .95, .99};

    @Bean
    public MetricsHandler ingesterCryptowatMetricsHandler(MeterRegistry meterRegistry) {
        Timer timer = Timer.builder(MetricsNames.INGESTER_CRYPTOWAT_OUT_METRIC)
                .publishPercentiles(DEFAULT_PERCENTILES)
                .register(meterRegistry);
        return new MetricsHandler(timer, CryptowatTradeMsg.EVENT, CryptowatTradeMsg.VERSION);
    }

    @Bean
    public MetricsHandler ingesterMetricsHandler(MeterRegistry meterRegistry) {
        Timer timer = Timer.builder(MetricsNames.INGESTER_OUT_METRIC)
                .publishPercentiles(DEFAULT_PERCENTILES)
                .register(meterRegistry);
        return new MetricsHandler(timer, CryptoTradeMsg.EVENT, CryptoTradeMsg.VERSION);
    }

    @Bean
    public MetricsHandler aggSummaryMetricsHandler(MeterRegistry meterRegistry) {
        Timer timer = Timer.builder(MetricsNames.AGG_SUMMARY_OUT_METRIC)
                .publishPercentiles(DEFAULT_PERCENTILES)
                .register(meterRegistry);
        return new MetricsHandler(timer, SummaryTradeMsg.EVENT, SummaryTradeMsg.VERSION);
    }

    @Bean
    public MetricsHandler aggMeanMetricsHandler(MeterRegistry meterRegistry) {
        Timer timer = Timer.builder(MetricsNames.AGG_MEAN_METRIC)
                .publishPercentiles(DEFAULT_PERCENTILES)
                .register(meterRegistry);
        return new MetricsHandler(timer, MeanTradePriceMsg.EVENT, MeanTradePriceMsg.VERSION);
    }

    @Bean
    public MetricsHandler sseUpdatedMetricsHandler(MeterRegistry meterRegistry) {
        Timer timer = Timer.builder(MetricsNames.SSE_UPDATED_METRIC)
                .publishPercentiles(DEFAULT_PERCENTILES)
                .register(meterRegistry);
        return new MetricsHandler(timer, SseUpdatedEventMsg.EVENT, SseUpdatedEventMsg.VERSION);
    }
}
