package com.woblak.cstream.metrics.core.io.kafka.consumer;

import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.metrics.core.io.kafka.handler.MsgHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@KafkaListener(
        topics = {
                KafkaTopics.CRYPTOWAT_TRADES,
                KafkaTopics.CRYPTO_TRADES,
                KafkaTopics.CRYPTO_AGG_SUMMARY,
                KafkaTopics.CRYPTO_AGG_MEAN,
                KafkaTopics.SSE_NOTIFICATIONS
        },
        containerFactory = "kafkaListenerContainerFactory"
)
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptoStreamListener {

    private final Set<MsgHandler> headersHandlers;

    @KafkaHandler(isDefault = true)
    public void listen(@Headers MessageHeaders headers) {
        try {
            handle(headers, headersHandlers);
        } catch (Exception e) {
            log.error(headers + e.getMessage(), e);
        }
    }

    private void handle(MessageHeaders headers, Set<MsgHandler> handlers) {
        handlers.stream()
                .filter(h -> h.test(headers))
                .forEach(h -> h.accept(headers));
    }
}
