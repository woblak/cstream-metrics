package com.woblak.cstream.metrics.core.io.kafka.handler;

import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.metrics.core.util.DeserializerUtils;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageHeaders;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class MetricsHandler implements MsgHandler {

    private final Timer timer;
    private final String event;
    private final String version;

    @Override
    public boolean test(MessageHeaders headers) {
        return event.equals(DeserializerUtils.getStringHeader(headers, KafkaHeaders.EVENT))
                && version.equals(DeserializerUtils.getStringHeader(headers, KafkaHeaders.VERSION));
    }

    @Override
    public void accept(MessageHeaders headers) {
        long outTimestamp = DeserializerUtils.getLongHeader(headers, KafkaHeaders.RECEIVED_TIMESTAMP);
        long inTimestamp = DeserializerUtils.getLongHeader(headers, KafkaHeaders.LAST_TIMESTAMP);
        timer.record(outTimestamp - inTimestamp, TimeUnit.MILLISECONDS);
    }
}
