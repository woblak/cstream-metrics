package com.woblak.cstream.metrics.core.io.kafka.config;


import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ListenerUtils;
import org.springframework.kafka.listener.RecordInterceptor;
import org.springframework.kafka.listener.adapter.RecordFilterStrategy;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
@Slf4j
public class KafkaConsumerConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;

    @Value("${spring.kafka.consumer.auto-offset-reset}")
    private String autoOffsetReset;

    @Value("${spring.kafka.consumer.ack-discarded:true}")
    private boolean ackDiscarded;

    @Value("${spring.kafka.consumer.enableAutoCommit:false}")
    private boolean enableAutoCommit;

    @Bean("kafkaListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory(
            ConsumerFactory<String, Object> consumerFactory,
            RecordInterceptor<String, Object> recordInterceptor,
            RecordFilterStrategy<String, Object> recordFilterStrategy,
            @Value("${spring.kafka.consumer.ack-discarded:true}") boolean ackDiscarded
    ) {
        ConcurrentKafkaListenerContainerFactory<String, Object> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        factory.setRecordInterceptor(recordInterceptor);
        factory.setAckDiscarded(ackDiscarded);
        factory.setRecordFilterStrategy(recordFilterStrategy);
        return factory;
    }

    @Bean
    public ConsumerFactory<String, Object> consumerFactory(
            @Qualifier("consumerKeyDeserializer") Deserializer<String> keyDeserializer,
            @Qualifier("consumerValueDeserializer") Deserializer<Object> valueDeserializer
    ) {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), keyDeserializer, valueDeserializer);
    }

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configs.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        configs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit);
        return configs;
    }

    @Bean
    public Deserializer<String> consumerKeyDeserializer() {
        return new ErrorHandlingDeserializer<>(new StringDeserializer());
    }

    @Bean
    public Deserializer<Object> consumerValueDeserializer() {
        return (s, bytes) -> bytes;
    }

    @Bean
    public DefaultKafkaHeaderMapper kafkaHeaderMapper() {
        return new DefaultKafkaHeaderMapper();
    }

    @Bean
    public RecordInterceptor<String, Object> recordInterceptor() {
        return consumerRecord -> {
            String str = ListenerUtils.recordToString(consumerRecord);
            log.debug("<< kafka << {}", str);
            return consumerRecord;
        };
    }

    @Bean
    RecordFilterStrategy<String, Object> recordFilterStrategy() {
        return record -> {
            return false;
        };
    }
}