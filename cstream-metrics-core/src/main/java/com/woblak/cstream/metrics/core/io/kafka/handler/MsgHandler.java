package com.woblak.cstream.metrics.core.io.kafka.handler;

import org.springframework.messaging.MessageHeaders;

public interface MsgHandler {

    boolean test(MessageHeaders headers);

    void accept(MessageHeaders headers);
}
