package com.woblak.cstream.metrics.core.util;

import lombok.experimental.UtilityClass;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

@UtilityClass
public class SerializerUtils {

    public static byte[] stringToByteArray(String s) {
        byte[] bytes;

        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(baos)
        ) {
            out.writeObject(s);
            out.flush();
            bytes = baos.toByteArray();
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }

        return bytes;
    }
}
